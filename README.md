# Scaleway Node Replacer

### To work properly specific environment variables must be defined:

* NODE_NAME
* SCALEWAY_NODE_REPLACEMENT_API_KEY
* DATACENTER_REGION
* K8S_CLUSTER_ID

NODE_NAME must be pass to the container using k8s Downward API.
See: https://kubernetes.io/docs/tasks/inject-data-application/environment-variable-expose-pod-information/

/!\ SCALEWAY_NODE_REPLACEMENT_API_KEY contain an open bar API KEY. Fear the wrath of god! /!\


### Celery worker management

The node_replacer can also properly stop a local celery worker if needed.
To do so pass `kill_celery=True` to the main class.

## How to

1. Be sure to add library in requirements.txt
```
-e git+ssh://git@gitlab.com/bodzen/krakeno/scaleway_node_replacer@omega-X#egg=scaleway_node_replacer
```
2. Import and use
```
from scaleway_node_replacer import node_replacer
node_replacer(kill_celery=False).run()
```
