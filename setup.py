#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup  # pyre-ignore

setup(
    name='scaleway_node_replacer',
    version='1.0.0',
    author='dh4rm4',
    description='Library to replace Scaleway\'s node using Scaleway Kapsule API.',
    install_requires=[
        'requests',
        'sentry_python_integration @ git+ssh://git@gitlab.com/bodzen/common/sentry_python_integration@omega-13#egg=sentry_python_integration'],
    classifiers=[
        "Programming Language :: Python 3.8+",
        "Development Status :: Never ending",
        "Language :: English",
        "Operating System :: Debian based",
    ],
)
