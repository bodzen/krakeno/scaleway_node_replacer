#!/usr/bin/python3
# -*- coding: utf-8 -*-
import os
from signal import SIGINT
import requests
import json

from sentry_python_integration.common import CustomSentryException


__all__ = ['node_replacer']


class ScalewayApiError_CantFindNode(CustomSentryException):
    def build_msg(self, node_name: str):
        return 'Scaleway\'s did not return a node corresponding ' \
            f'to name: {node_name}.'


class ScalewayApiError_CannotReplaceNode(CustomSentryException):
    def build_msg(self, http_resp, node_id: str):
        return f'Could not replace node "{node_id}" due to API error: ' \
            f'{http_resp.status_code} -> {http_resp.text}'


class node_replacer:
    """
        Class used to requests a node replacement using Scaleway's API.
        Everything will be based on the env variable NODE_NAME, which should
        be present in the container.
    """
    def __init__(self, kill_celery=False):
        self.kill_celery = kill_celery
        self.node_name = os.environ['NODE_NAME']
        self.cluster_id = os.environ['K8S_CLUSTER_ID']
        self.api_base_endpoint = self.__get_api_base_endpoint()
        self.auth_header = self.__get_authentification_header()

    def __get_authentification_header(self) -> dict:
        """
            Build and return header used to authentificate to Scaleway's API
            based on SCALEWAY_NODE_REPLACEMENT_API_KEY env var
            return: (dict) auth header as dict
        """
        api_key = os.environ['SCALEWAY_NODE_REPLACEMENT_API_KEY']
        return {'X-Auth-Token': api_key}

    def __get_api_base_endpoint(self) -> str:
        """
            Build scaleway's kaspsule api endpoint using
            DATACENTER_REGION + CLUSTER_ID env var
            return: (str) api base endpoint
        """
        region = os.environ['DATACENTER_REGION']
        return f'https://api.scaleway.com/k8s/v1/regions/{region}/'

    def run(self):
        if self.kill_celery:
            self.__kill_local_celery_worker()
        self.__request_node_replacement()

    def __kill_local_celery_worker(self):
        """
            Send SIGINT to kill local celery worker
        """
        os.kill(1, SIGINT)

    def __request_node_replacement(self):
        """
            Request kapsule node replacement to Scaleway to update IP adress
        """
        node_id = self.__get_node_id()
        api_endpoint = self.api_base_endpoint + f'nodes/{node_id}/replace'
        resp = requests.post(api_endpoint,
                             headers=self.auth_header,
                             json={})
        if resp.status_code != 200:
            raise ScalewayApiError_CannotReplaceNode(resp, node_id)

    def __get_node_id(self) -> str:
        """
            Request Scaleway Kapsule's API to get the node's id
            corresponding the NODE_NAME
            return: (str) node id
        """
        cluser_nodes_infos = self.__get_cluster_nodes_infos()
        try:
            for node_infos in cluser_nodes_infos['nodes']:
                if node_infos['name'] == self.node_name:
                    return node_infos['id']
            raise ScalewayApiError_CantFindNode(self.node_name)
        except KeyError:
            raise ScalewayApiError_CantFindNode(self.node_name)

    def __get_cluster_nodes_infos(self):
        """
            Request Scaleway Kapsule's API to get nodes infos
            on given cluster
        """
        api_endpoint = self.api_base_endpoint + f'clusters/{self.cluster_id}/nodes'
        resp = requests.get(api_endpoint, headers=self.auth_header)
        return json.loads(resp.text)


if __name__ in '__main__':
    node_replacer().run()
